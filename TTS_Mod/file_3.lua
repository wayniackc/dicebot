--Runs whenever game is saved/autosaved
function onSave()
    local data_to_save = {rt=rollerToggle}
    saved_data = JSON.encode(data_to_save)
    --saved_data = "" --Remove -- at start + save to clear save data
    return saved_data
end

--Runs when game is loaded.
function onLoad(saved_data)
    --Loads the tracking for if the game has started yet
    --This recalls the state of the "toggle"
    if saved_data ~= "" then
        local loaded_data = JSON.decode(saved_data)
        rollerToggle = loaded_data.rt
    else
        rollerToggle = false
    end
    --This hides the roll buttons on launch if the tool was toggled off
    if rollerToggle then
        UI.setAttribute("rollerButtons", "active", false)
        UI.setAttribute("rollerLayout", "height", 40)
    end
    --If you intend to use this, I would recommend using math.randomseed here
end

--Hides the welcome message
function hideWelcome()
    UI.hide("welcome")
end

--This toggles showing or hiding the roll buttons
function toggleRollerButtons()
    if rollerToggle then
        --UI.show("rollerButtons")
        UI.setAttribute("rollerButtons", "active", true)
        UI.setAttribute("rollerLayout", "height", 280)
    else
        --UI.hide("rollerButtons")
        UI.setAttribute("rollerButtons", "active", false)
        UI.setAttribute("rollerLayout", "height", 40)
    end
    --This flips between true/false for show/hide
    rollerToggle = not rollerToggle
end

--Activated by roll buttons, this gets a random value and prints it
function rollDice(player, _, idValue)
    --idValue is the "id" value of the XML button
    roll = math.random(idValue)
    str = player.steam_name .. " rolled a " .. roll .. " out of " .. idValue
    broadcastToAll(str, {1,1,1})
end