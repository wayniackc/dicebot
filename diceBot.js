const { Client, MessageEmbed } = require('discord.js')
const { prefix, token } = require('./config.json')

const client = new Client()

// Display a message once the bot has started
client.once("ready", () =>{
    console.log(`Logged in as ${client.user.tag}!`)
})

// Check messages for a specific command
client.on("message", message =>{
    if (!message.content.startsWith(prefix) || message.author.bot) return
    
    const args = message.content.slice(prefix.length).split(' ')
    const command = args.shift().toLowerCase()

    if (command === "roll") {
        if (!args.length) {
            roll = Math.floor((Math.random() * 20) + 1)

            const embed = new MessageEmbed()
            .setTitle(`${message.member.displayName} rolled 1d20...`)
            .setColor(0xff0000)
            .setDescription(roll);
    
            return message.channel.send(embed)
        }
        
        let sides = 20
        let rolls = 1

        if (args[0].includes('d')) {
            rolls = args[0].split('d')[0]
            sides = args[0].split('d')[1]
        }

        if (rolls >= 1) {
            const rollResults = []
            for (let i = 0; i < rolls; i++) {
                rollResults.push(Math.floor((Math.random() * sides) + 1))
            }
            const sum = rollResults.reduce((a,b) => a + b)
            const embed = new MessageEmbed()
            .setTitle(`${message.member.displayName} rolled ${rolls}d${sides}...`)
            .setColor(0xff0000)
            .addField("Results:", `${rollResults}`, true)
            .addField("Total:", `${sum}`, true)

            return message.channel.send(embed)
        }
    } else if (command === "help") {
        const embed = new MessageEmbed()
        .setTitle("RollBot Commands")
        .setColor(0xff0000)
        .addFields(
            { name: '!roll', value: 'Rolls 1d20'},
            { name: '!help', value: 'Displays available commands'}
        )
        message.channel.send('Available commands are:\n`!roll` - rolls 1d20\n`!help` - displays available commands')
    }
})

// Log in the bot with the token
client.login(token)